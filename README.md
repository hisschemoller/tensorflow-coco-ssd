# tensorflow-coco-ssd

Object Detection (coco-ssd)\
https://github.com/tensorflow/tfjs-models/tree/master/coco-ssd

TensorFlow.js — Real-Time Object Detection in 10 Lines of Code
https://medium.com/hackernoon/tensorflow-js-real-time-object-detection-in-10-lines-of-code-baf15dfb95b2

Base model 'mobilenet_v2' geeft duidelijk zichtbaaar beter resultaat dan de default 'lite_mobilenet_v2'.

## App

Er zijn een `processVideo` en `processImageSequence` functie die de video analyseren en een JSON
bestand maken van het resultaat, dat aan het eind als download verschijnt.\
De image sequence is later gemaakt omdat die beter met het proces te synchroniseren is dan een video
bestand.

De `playVideo` en `playImageSequence` functies spelen de video af samen met de bounding boxes uit
het analyse JSON bestand, om de resultaten nog eens te kunnen bekijken.\
Beide roepen `processData` en `processMergeData` aan waarin aangegeven paden (overeenkomende gedetecteerde objecten
van frame tot frame) gecombineerd worden tot langere.

* playImageSequence
  * processData
  * processMergeData


## Video's converteren

```bash
ffmpeg -ss 00:00:01.0 -i 'public/video/Berlijn Bülowstraße 2023-09-17 6114.mp4' -c copy -t 00:00:01.0 'public/video/bulowstrasse-01-01.mp4'
ffmpeg -i 'public/video/Berlijn Bülowstraße 2023-09-17 6114.mov' -f mp4 -vcodec libx264 -pix_fmt yuv420p 'public/video/Berlijn Bülowstraße 2023-09-17 6114.mp4'
```

```bash
ffmpeg -ss 00:00:00.0 -i "public/video/Parijs Gare de l'Est 2021-09-24 2883.mov" -c copy -t 00:00:19.0 'public/video/gare-de-lest.mov'
ffmpeg -i 'public/video/gare-de-lest.mov' -f mp4 -vcodec libx264 -pix_fmt yuv420p 'public/video/gare-de-lest.mp4'
```

```bash
ffmpeg -ss 00:00:34.0 -i "public/video/Weesperplein 2022-08-19.mov" -c copy -t 00:00:20.0 'public/video/weesperplein.mov'
ffmpeg -i 'public/video/weesperplein.mov' -f mp4 -vcodec libx264 -pix_fmt yuv420p 'public/video/weesperplein.mp4'
```

```bash
# circa 80% smaller filesize
ffmpeg -i 'public/video/weesperplein.mov' -f mp4 -vcodec libx264 -pix_fmt yuv420p 'public/video/weesperplein.mp4'
# without quality loss, circa 300% increased file size
ffmpeg -i 'public/video/weesperplein.mov' -crf 1 -c:v libx264 'public/video/weesperplein.mp4'
```
