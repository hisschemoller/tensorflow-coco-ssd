import { DetectedObject } from '@tensorflow-models/coco-ssd';
import processData, { DetectedObjectExt, download } from './process-data';

const colors = [
  '#00ff00',
  '#ff0000',
  '#0000ff',
  '#00ffff',
  '#ffff00',
  '#ff00ff',
  '#ffcc66',
  '#ffffff',
  '#555555',
  '#aaaaaa',
];
let fps: number;
let frameIndex = 0;
let lastFrameIndex: number;
let video: HTMLVideoElement;
let width: number;
let height: number;
let ctx: CanvasRenderingContext2D;
let rafCount = 0;
let data: DetectedObject[][];
let dataExt: DetectedObjectExt[][];

function processVideoFrame() {
  rafCount++;
  if (rafCount % 2 === 0) {
    requestAnimationFrame(processVideoFrame);
    return;
  }

  if (video.ended) {
    console.log('done');
    return;
  }

  const time = frameIndex / fps;
  if (time > video.duration) {
    console.log('ended');
    return;
  }

  if (frameIndex > lastFrameIndex) {
    console.log('lastframe');
    return;
  }

  ctx.drawImage(video, 0, 0, width, height);

  dataExt[frameIndex + 2].forEach((detObj) => {
    const { bbox, id } = detObj;
    const [x, y, width, height] = bbox;
    ctx.strokeStyle = colors[id % colors.length];
    ctx.fillStyle = colors[id % colors.length];
    ctx.strokeRect(x, y, width, height);
    ctx.strokeRect(x, y - 50, Math.max(width, 100), 50);
    ctx.fillRect(x, y - 50,  Math.max(width, 100), 50);
    ctx.fillStyle = '#000000';
    ctx.fillText(id.toString(), x + 10, y - 10);
  });

  frameIndex++;

  requestAnimationFrame(processVideoFrame);
}

function pause() {
  
}

/**
 * Play video and its detection data.
 */
export default async function playVideo(
  videoPath: string,
  jsonPath: string,
  _fps: number,
  _width: number,
  _height: number,
) {
  fps = _fps;
  width = _width;
  height = _height;

  const canvasEl = document.getElementById('canvas') as HTMLCanvasElement;
  const videoDivEl = document.getElementById('video') as HTMLDivElement;

  ctx = canvasEl.getContext('2d', {
    willReadFrequently: true,
  }) as CanvasRenderingContext2D;

  const response = await fetch(jsonPath);
  data = await response.json();
  dataExt = processData(data);

  video = document.createElement('video');
  videoDivEl.appendChild(video);
  video.src = videoPath;
  video.addEventListener('play', () => {
    console.log('play');
    ctx.font = 'bold 40px sans-serif';
    ctx.lineWidth = 8;
    lastFrameIndex = Math.floor(video.duration * fps);

    requestAnimationFrame(processVideoFrame);
  });
  video.addEventListener('pause', () => {
    console.log('pause');
  })

  document.getElementById('startbutton')?.addEventListener('click', () => video.play());
  document.getElementById('pausebutton')?.addEventListener('click', () => pause());
  document.getElementById('savebutton')?.addEventListener('click', () => download(dataExt, 'motiondata.json'));
}
