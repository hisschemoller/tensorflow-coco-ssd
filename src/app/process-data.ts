import { DetectedObject } from '@tensorflow-models/coco-ssd';

export type DetectedObjectExt = DetectedObject & {
  id: number;
}

const X_DIST = 40;
const Y_DIST = 30;
const GAP_DURATION = 50;
const MIN_DURATION = 60;

const durations: number[] = [];
let currentId = 0;

export function download(content: any[][], fileName: string) {
  const jsonString = JSON.stringify(content, null, 2);
  const a = document.createElement('a');
  const file = new Blob([jsonString], { type: 'text/plain' });
  a.href = URL.createObjectURL(file);
  a.download = fileName;
  a.click();
}

function getId() {
  const id = currentId;
  currentId++;
  return id;
}

export default function processData(data: DetectedObject[][]) {
  
  // convert DetectedObject[][] to DetectedObjectExt[][]
  const dataExt = data.map((objectsInFrame) => {
    return objectsInFrame.map((objectInFrame) => ({ ...objectInFrame, id: -1 } as DetectedObjectExt));
  });

  dataExt.forEach((objectsInFrame, index) => {
    if (index === 0) {

      // first frame objects just get ids
      objectsInFrame.forEach((objectInFrame) => objectInFrame.id = getId())

      // add each dataExt id to the durations array
      objectsInFrame.forEach((objectInFrame) => durations[objectInFrame.id] = 0)
    } else  {

      // further frames compare to the previous frame
      const previousFrameIndex = index - 1;
      const objectsInPreviousFrame = dataExt[previousFrameIndex];

      objectsInFrame.forEach((objectInFrame) => {

        // search for object in close proximity
        const [ x, y, width, height ] = objectInFrame.bbox;
        const match = objectsInPreviousFrame.find((p) => {
          const [ px, py, pwidth, pheight ] = p.bbox;
          return Math.abs(px - x) < X_DIST && Math.abs(py - y) < Y_DIST;
        });

        // if found assign the same id
        if (match) {
          objectInFrame.id = match.id;
          durations[objectInFrame.id] += 1;
        }
      });

      // all objects that didn't match get a new id
      objectsInFrame.forEach((objectInFrame) => {
        if (objectInFrame.id === -1) {
          objectInFrame.id = getId();
          durations[objectInFrame.id] = 0;
        }
      });

      // all previous objects that are not found anymore in the current frame
      // continue at their position for several frames to bridge short continuity gaps
      objectsInPreviousFrame.forEach((p) => {
        
        // search for current object with the same id
        const match = objectsInFrame.find((obj) => obj.id === p.id);
        if (!match) {

          // now check for how long this object 'p' has not been moving
          let matchCount = 1;
          for (let i = 2, n = GAP_DURATION; i <= n; i += 1) {

            const objectsInFrameBefore = dataExt[index - i];
            if (objectsInFrameBefore) {
              const matchBefore = objectsInFrameBefore.find((obj) => obj.id === p.id);
              if (matchBefore) {
                matchCount++;
              }
            }
          }

          // the object can wait a little longer
          if (matchCount < GAP_DURATION) {
            objectsInFrame.push(p);
            durations[p.id] += 1;
          } else {

            // object has been static too long, so it's finished, remove its tail
            let indexGoingBackwards = previousFrameIndex;
            let isTail = true;
            while(isTail) {
              const tailObjectsInFrame = dataExt[indexGoingBackwards];
              const tailObjectIndex = tailObjectsInFrame.findIndex((obj) => {
                return obj.id === p.id && obj.bbox[0] === p.bbox[0] && obj.bbox[1] === p.bbox[1];
              });

              if (tailObjectIndex !== -1) {

                // lower the duration
                durations[tailObjectsInFrame[tailObjectIndex].id] -= 1;

                // remove the found tail
                tailObjectsInFrame.splice(tailObjectIndex, 1);
                indexGoingBackwards--;

                // quit if first frame reached
                if (indexGoingBackwards < 0) {
                  isTail = false;
                }
              } else {

                // tail ended so quit
                isTail = false;
              }
            }
          }
        }
      });

      // detecteer uit het verleden de richting en snelheid van een object,
      // en zoek vervolgens naar verplaatsing specifiek in die richting
    }
  });

  // const longObjects = durations.reduce((a, d) => d < MIN_DURATION ? a : a + 1, 0);
  // console.log(`Total IDs used: ${currentId}`);
  // console.log(`Durations: ${durations}`);
  // console.log(`Durations: ${durations.length}`);
  // console.log(`Objects with long enough duration: ${longObjects}`);

  // remove the objects that exist for too short a time
  dataExt.forEach((objectsInFrame) => {
    objectsInFrame.reduce((accumulator, objectInFrame) => {
      const duration = durations[objectInFrame.id];
      return duration < MIN_DURATION ? accumulator : [...accumulator, objectInFrame];
    }, [] as DetectedObjectExt[]);
  });

  let minX = 10000;
  let maxX = 0;
  dataExt.forEach((objectsInFrame, index) => {
    objectsInFrame.forEach((objectInFrame) => {
      minX = Math.min(minX, objectInFrame.bbox[0]);
      maxX = Math.max(maxX, objectInFrame.bbox[0]);
    });
  });

  console.log(`minX: ${minX}`);
  console.log(`maxX: ${maxX}`);

  return dataExt;
}
