import { DetectedObject } from '@tensorflow-models/coco-ssd';
import processData, { DetectedObjectExt, download } from './process-data';
import processMergeData from './process-merge-data';

const colors = [
  '#00ff00',
  '#ff0000',
  '#0000ff',
  '#00ffff',
  '#ffff00',
  '#ff00ff',
  '#ffcc66',
  '#ffffff',
  '#555555',
  '#aaaaaa',
];
let fps: number;
let frameIndex = 1;
let lastFrameIndex: number;
let img: HTMLImageElement;
let imgSeqPath: string;
let width: number;
let height: number;
let ctx: CanvasRenderingContext2D;
let rafCount = 0;
let data: DetectedObject[][];
let dataExt: DetectedObjectExt[][];
let pathsToMerge: number[][];

async function loadVideoFrame(index: number) {
  return new Promise<boolean>((resolve, reject) => {
    img.onload = () => resolve(true);
    img.onerror = reject;
    img.src = imgSeqPath
      .split('#FRAME#')
      .join((index <= 99999) ? (`0000${Math.round(index)}`).slice(-5) : '99999');
  })
}

/**
 * Play video frame by frame.
 */
async function playVideoFrames() {
  rafCount++;
  if (rafCount % 2 === 0) {
    requestAnimationFrame(playVideoFrames);
    return;
  }

  let success = false;
  try {
    success = await loadVideoFrame(frameIndex);
  } catch {}

  if (!success) {
    console.log('Error at frame', frameIndex);
    return;
  }

  ctx.drawImage(img, 0, 0, width, height);

  frameIndex++;
  
  dataExt[frameIndex + 2].forEach((detObj) => {
    const { bbox, id } = detObj;
    const [x, y, width, height] = bbox;
    ctx.strokeStyle = colors[id % colors.length];
    ctx.fillStyle = colors[id % colors.length];
    ctx.strokeRect(x, y, width, height);
    ctx.strokeRect(x, y - 50, Math.max(width, 100), 50);
    ctx.fillRect(x, y - 50,  Math.max(width, 100), 50);
    ctx.fillStyle = '#000000';
    ctx.fillText(id.toString(), x + 10, y - 10);
  });

  frameIndex++;

  requestAnimationFrame(playVideoFrames);
}

/**
 * Detect objects in a video.
 */
export default async function playImageSequence(
  _imgSeqPath: string,
  jsonPath: string,
  _pathsToMerge: number[][],
  _fps: number,
  _width: number,
  _height: number,
) {
  fps = _fps;
  imgSeqPath = _imgSeqPath;
  pathsToMerge = _pathsToMerge;
  width = _width;
  height = _height;

  const canvasEl = document.getElementById('canvas') as HTMLCanvasElement;
  const videoDivEl = document.getElementById('video') as HTMLDivElement;

  ctx = canvasEl.getContext('2d', {
    willReadFrequently: true,
  }) as CanvasRenderingContext2D;
  ctx.font = 'bold 40px sans-serif';
  ctx.lineWidth = 8;

  const response = await fetch(jsonPath);
  data = await response.json();
  dataExt = processData(data);
  processMergeData(dataExt, pathsToMerge);

  img = new Image();
  videoDivEl.appendChild(img);

  document.getElementById('startbutton')?.addEventListener('click', () => playVideoFrames());
  document.getElementById('savebutton')?.addEventListener('click', () => download(dataExt, 'motiondata.json'));
}