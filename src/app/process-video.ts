import { DetectedObject, ObjectDetection } from '@tensorflow-models/coco-ssd';
import { download } from './process-data';

const MAX_NUM_BOXES = 12;
const MIN_SCORE = 0.6;

/**
 * Detect objects in an video frame by frame.
 */
async function processVideoFrames(
  video: HTMLVideoElement,
  ctx: CanvasRenderingContext2D,
  model: ObjectDetection,
  data: DetectedObject[][],
  fps: number,
  width: number,
  height: number,
) {
  let frameIndex = 0;
  let lastFrameIndex = Math.floor(video.duration * fps);
  ctx.lineWidth = 2;
  ctx.strokeStyle = '#00ff00';

  const intervalId = setInterval(async () => {
    if (video.ended) {
      clearInterval(intervalId);
      console.log('done');
      download(data, 'bulowstrasse.json');
      return;
    }

    const time = frameIndex / fps;
    if (time > video.duration) {
      clearInterval(intervalId);
      console.log('ended');
      download(data, 'bulowstrasse.json');
      return;
    }

    if (frameIndex > lastFrameIndex) {
      clearInterval(intervalId);
      console.log('lastframe');
      download(data, 'bulowstrasse.json');
      return;
    }
    
    video.currentTime = time;
    ctx.drawImage(video, 0, 0, width, height);
    console.log('frameIndex', frameIndex);
    frameIndex++;

    const imageData = ctx.getImageData(0, 0, width, height);
  
    const predictions = await model.detect(imageData, MAX_NUM_BOXES, MIN_SCORE);
  
    predictions.forEach((prediction) => {
      const { bbox } = prediction;
      const [x, y, width, height] = bbox;
      ctx.strokeRect(x, y, width, height);
    });

    data.push(predictions);
  }, 500);
}

/**
 * Detect objects in an video.
 */
export default function processVideo(
  videoPath: string,
  model: ObjectDetection,
  fps: number,
  width: number,
  height: number,
) {
  const canvasEl = document.getElementById('canvas') as HTMLCanvasElement;
  const videoDivEl = document.getElementById('video') as HTMLDivElement;
  const ctx = canvasEl.getContext('2d', {
    willReadFrequently: true,
  }) as CanvasRenderingContext2D;
  const data: DetectedObject[][] = [];

  const video = document.createElement('video');
  videoDivEl.appendChild(video);
  video.src = videoPath;
  video.addEventListener('play', () => {
    console.log('play');
    video.pause();
    processVideoFrames(video, ctx, model, data, fps, width, height);
  });
  video.addEventListener('pause', () => {
    console.log('pause');
  })

  document.getElementById('startbutton')?.addEventListener('click', () => video.play());
}
