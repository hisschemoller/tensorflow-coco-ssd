import { DetectedObjectExt } from './process-data';

function mergePaths(
  pathId: number,
  nextPathId: number,
  data: DetectedObjectExt[][],
  isTheOne: boolean,
) {
  // find last DetectedObjectExt with pathId
  const lastOfPathFrameIndex = data.findLastIndex((frame) => frame.find((o) => o.id === pathId));

  // find first DetectedObjectExt with nextPathId
  let firstOfNextPathFrameIndex = data.findIndex((frame) => frame.find((o) => o.id === nextPathId));

  if (isTheOne) {
    // console.log('nextPathId', nextPathId);
    // console.log('firstOfNextPathFrameIndex', firstOfNextPathFrameIndex);
    const objExt = data[firstOfNextPathFrameIndex].find((o) => o.id === nextPathId);
    if (objExt) {
      console.log('objExt', nextPathId, objExt.bbox[0]);
    }
  }

  // if the paths overlap
  if (firstOfNextPathFrameIndex <= lastOfPathFrameIndex) {

    // remove nextpath from all overlappting frames
    for (let i = firstOfNextPathFrameIndex, n = lastOfPathFrameIndex; i <= n; i++) {
      const removeIndex = data[i].findIndex((o) => o.id === nextPathId);
      if (removeIndex !== -1) {
        data[i].splice(removeIndex, 1);
      }
    } 

    // adjust nextPath's start index
    firstOfNextPathFrameIndex = data.findIndex((frame) => frame.find((o) => o.id === nextPathId));
  }

  // if empty frames inbetween
  if (firstOfNextPathFrameIndex - lastOfPathFrameIndex > 1) {

    // fill empty frames inbetween with copies of last DetectedObjectExt
    // const lastOfPathObject = data[lastOfPathFrameIndex].find((o) => o.id === pathId);
    // if (lastOfPathObject) {
    //   for (let i = lastOfPathFrameIndex + 1, n = firstOfNextPathFrameIndex; i < n; i++) {
    //     data[i].push({ ...lastOfPathObject, bbox: [...lastOfPathObject.bbox]});
    //   }
    // }

    // if empty frames inbetween lerp between the two bounding boxes
    const numEmptyFrames = firstOfNextPathFrameIndex - lastOfPathFrameIndex;
    if (numEmptyFrames > 1) {
      const lastOfPathObject = data[lastOfPathFrameIndex].find((o) => o.id === pathId);
      const firstOfNextPathObject = data[firstOfNextPathFrameIndex].find((o) => o.id === nextPathId);
      if (lastOfPathObject && firstOfNextPathObject) {
        // for (let i = lastOfPathFrameIndex + 1, n = firstOfNextPathFrameIndex; i < n; i++) {
        for (let i = 0, n = numEmptyFrames; i < n; i++) {
          const bbStart = lastOfPathObject.bbox;
          const bbEnd = firstOfNextPathObject.bbox;
          const alpha = i / numEmptyFrames;
          data[lastOfPathFrameIndex + 1 + i].push({
            ...lastOfPathObject,
            bbox: [
              bbStart[0] + ((bbEnd[0] - bbStart[0]) * alpha),
              bbStart[1] + ((bbEnd[1] - bbStart[1]) * alpha),
              bbStart[2] + ((bbEnd[2] - bbStart[2]) * alpha),
              bbStart[3] + ((bbEnd[3] - bbStart[3]) * alpha),
            ],
          });
      }
      }
    }
  }

  // all frames with nextPathId get their id set to pathId
  data.forEach((arr) => {
    const obj = arr.find((o) => o.id === nextPathId);
    if  (obj) {
      obj.id = pathId;
    }
  });

  // search for periods of no movement in paths and lerp those
}

/**
 * Merge paths
 */
export default function processMergeData(
  data: DetectedObjectExt[][],
  pathsToMerge: number[][],
) {
  pathsToMerge.forEach((pathToMerge) => {
    const isTheOne = pathToMerge[0] === 57;
    for (let i = pathToMerge.length - 1; i > 0; i--) {
      mergePaths(pathToMerge[i -  1], pathToMerge[i], data, isTheOne);
    }
  });
}
