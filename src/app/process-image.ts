import { ObjectDetection } from '@tensorflow-models/coco-ssd';

/**
 * Detect objects in an image.
 */
export default function processImage(model: ObjectDetection, imgPath: string) {
  const canvasEl = document.getElementById('canvas') as HTMLCanvasElement;
  const ctx = canvasEl.getContext('2d') as CanvasRenderingContext2D;
  let imageData: ImageData;

  const image = new Image();
  image.src = imgPath;
  image.onload = async () => {
    ctx.drawImage(image, 0, 0);
    imageData = ctx.getImageData(0, 0, 1920, 1080);

    ctx.strokeStyle = 'blue';
    ctx.strokeRect(10, 10, 1900, 1060);
    ctx.lineWidth = 2;
    ctx.strokeStyle = '#ff0000';
  
    const predictions = await model.detect(imageData);
  
    console.log('Predictions: ');
    console.log(predictions);
  
    predictions.forEach((prediction) => {
      const { bbox } = prediction;
      const [x, y, width, height] = bbox;
      console.log(x, y, width, height);
      ctx.strokeRect(x, y, width, height);
    });
  }
}
