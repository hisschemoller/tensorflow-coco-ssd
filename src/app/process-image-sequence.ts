import { DetectedObject, ObjectDetection } from '@tensorflow-models/coco-ssd';
import { download } from './process-data';

const MAX_NUM_BOXES = 12;
const MIN_SCORE = 0.6;

async function loadVideoFrame(index: number, img: HTMLImageElement, imgSeqPath: string) {
  return new Promise<boolean>((resolve, reject) => {
    img.onload = () => resolve(true);
    img.onerror = reject;
    img.src = imgSeqPath
      .split('#FRAME#')
      .join((index <= 99999) ? (`0000${Math.round(index)}`).slice(-5) : '99999');
  })
}

/**
 * Detect objects in an video frame by frame.
 */
function processVideoFrames(
  imgSeqPath: string,
  img: HTMLImageElement,
  ctx: CanvasRenderingContext2D,
  model: ObjectDetection,
  data: DetectedObject[][],
  width: number,
  height: number,
) {
  let frameIndex = 1;
  ctx.lineWidth = 2;
  ctx.strokeStyle = '#00ff00';
  

  const intervalId = setInterval(async () => {
    let success = false;
    try {
      success = await loadVideoFrame(frameIndex, img, imgSeqPath);
    } catch {}
    
    if (!success) {
      console.log('No success loading image.');
      clearInterval(intervalId);
      download(data, 'motiondata-original.json');
      return;
    }
    
    ctx.drawImage(img, 0, 0, width, height);
    console.log('frameIndex', frameIndex);
    frameIndex++;

    const imageData = ctx.getImageData(0, 0, width, height);
  
    const predictions = await model.detect(imageData, MAX_NUM_BOXES, MIN_SCORE);
  
    predictions.forEach((prediction) => {
      const { bbox } = prediction;
      const [x, y, width, height] = bbox;
      ctx.strokeRect(x, y, width, height);
    });

    data.push(predictions);
  }, 500);
}

/**
 * Detect objects in an video.
 */
export default function processImageSequence(
  imgSeqPath: string,
  model: ObjectDetection,
  fps: number,
  width: number,
  height: number,
) {
  const canvasEl = document.getElementById('canvas') as HTMLCanvasElement;
  const videoDivEl = document.getElementById('video') as HTMLDivElement;
  const ctx = canvasEl.getContext('2d', {
    willReadFrequently: true,
  }) as CanvasRenderingContext2D;
  const data: DetectedObject[][] = [];

  const img = new Image();
  videoDivEl.appendChild(img);

  document.getElementById('startbutton')?.addEventListener('click', () => {
    processVideoFrames(imgSeqPath, img, ctx, model, data, width, height);
  });
}
