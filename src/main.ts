import './style.css';
import '@tensorflow/tfjs-backend-cpu';
import '@tensorflow/tfjs-backend-webgl';
import { load as loadObjectDetection } from '@tensorflow-models/coco-ssd';
import processImage from './app/process-image';
import processVideo from './app/process-video';
import playVideo from './app/play-video';
import processImageSequence from './app/process-image-sequence';
import playImageSequence from './app/play-image-sequence';

/**
 * Autostart.
 */
(async () => {
  // const model = await loadObjectDetection({
  //   base: 'mobilenet_v2',
  // });
  // console.log('Model loaded.')

  // const videoPath = '/video/weesperplein.mp4';
  // processVideo(videoPath, model, 30, 1920, 1080);

  const imgSeqPath = '/video/frames/frame_#FRAME#.png';
  // processImageSequence(imgSeqPath, model, 30, 1920, 1080);

  const jsonPath = '/json/weesperplein-original-2.json';
  // playVideo(videoPath, jsonPath, 30, 1920, 1080);

  const pathsToMerge = [
    [4, 16, 21, 30], // auto
    [57, 59, 61, 62, 65, 68, 69, 75, 84, 96, 100, 114, 103], // tram
    [145, 152, 161, 177, 178, 180, 191, 205, 208], // vrouw
    [9, 11, 32, 64, 82, 89, 103, 151], // zwarte auto wachtend voor stoplicht
    [97, 49, 113, 116, 129], // zwarte bus wachtend achter de auto
    [111, 127, 146, 174], // derde wachtende auto
    [156, 157, 159, 160, 162, 163, 168, 170, 172, 173, 176, 179, 187], // rode vrachtwagen
    [131, 153], // witte bus
    [202, 203, 204, 207, 208, 212, 213, 219, 222], // eerste auto van rechts
    [216, 224, 227, 230, 233, 244], // tweede auto van rechts
    [226, 239], // derde auto van rechts
    [256, 269, 282, 283, 287, 289, 293, 294], // 
    [319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351], // vooraan snel
  ];
  playImageSequence(imgSeqPath, jsonPath, pathsToMerge, 30, 1920, 1080);

  // const imgPath = '/video/Muiderpoortstation Omgeving 14 2023-09-08.jpg';
  // processImage(model, imgPath);
})();
